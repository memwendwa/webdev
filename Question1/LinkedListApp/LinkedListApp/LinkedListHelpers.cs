﻿using System;
using System.Collections.Generic;

namespace LinkedListApp
{
    public static class LinkedListHelpers
    {
        public static void DeleteDuplicates(LinkedList rootNode)
        {
            var nodes = new Dictionary<char, int>();//This dictionary keeps counters for the encountered values
            LinkedList currentNode = rootNode, previousNode= currentNode;
            while(currentNode != null)
            {
                //previousNode = currentNode;
                if (nodes.ContainsKey(currentNode.Value))//check if current node value has been encountered before
                {
                    var dicValue = nodes[currentNode.Value];
                    if (dicValue > 1)//The current value has been encontered 2 times already, so delete the current Node
                    {
                        previousNode.Next = currentNode.Next;
                        currentNode = previousNode.Next;
                    }
                    else
                    {
                        nodes[currentNode.Value] += 1; //Increment counter for the current value
                        previousNode = currentNode;
                        currentNode = currentNode.Next;// move to next node
                    }
                }
                else//It the first time we are encountering the current value
                {
                    nodes.Add(currentNode.Value, 1); //Add counter for the current value
                    previousNode = currentNode;
                    currentNode = currentNode.Next;
                }
            };
        }

        /// <summary>
        /// Helper Method to add new node at the end
        /// </summary>
        /// <param name="root"></param>
        /// <param name="value"></param>
        public static void AddToLast(LinkedList root, Char value)
        {
            if (root == null) return;
            LinkedList current = root.Next;
            if(current == null)
            {
                root.Next = new LinkedList(value);
                return;
            }
            while (current.Next != null)
            {
                current = current.Next;
            }
            current.Next = new LinkedList(value);
        }

        /// <summary>
        /// Helper method to display all nodes values
        /// </summary>
        /// <param name="root"></param>
        public static void Display(LinkedList root)
        {
            if(root == null)
            {
                return;
            }
            Console.WriteLine(root.Value);
            LinkedList current = root.Next;
            
            while(current != null)
            {
                Console.WriteLine(current.Value);
                current = current.Next;
            }
        }
        /// <summary>
        /// Helper method to return nodes values as string
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        public static string GetNodesString(LinkedList root)
        {
            var nodeSting = string.Empty;
            if(root == null)
            {
                return nodeSting;
            }
            nodeSting += root.Value;
            LinkedList current = root.Next;
            while(current != null)
            {
                nodeSting += current.Value;
                current = current.Next;
            }
            return nodeSting;
        }
    }

    public class LinkedList
    {
        public LinkedList(Char value)
        {
            Value = value;
        }
        public Char Value { get; private set; }
        public LinkedList Next { get; set; }


    }
}
