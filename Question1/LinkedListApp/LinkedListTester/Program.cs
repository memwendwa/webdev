﻿using LinkedListApp;
using System;

namespace LinkedListTester
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var root = new LinkedList('E');
            LinkedListHelpers.AddToLast(root, 'B');
            LinkedListHelpers.AddToLast(root, 'E');
            LinkedListHelpers.AddToLast(root, 'E');
            LinkedListHelpers.AddToLast(root, 'B');
            LinkedListHelpers.AddToLast(root, 'A');
            LinkedListHelpers.AddToLast(root, 'B');

            Console.WriteLine("Nodes Before duplicates removal");
            LinkedListHelpers.Display(root);

            Console.WriteLine(LinkedListHelpers.GetNodesString(root));

            LinkedListHelpers.DeleteDuplicates(root);

            Console.WriteLine("Nodes After duplicates removal");
            LinkedListHelpers.Display(root);
            Console.WriteLine(LinkedListHelpers.GetNodesString(root));

            Console.ReadLine();

        }
    }
}
