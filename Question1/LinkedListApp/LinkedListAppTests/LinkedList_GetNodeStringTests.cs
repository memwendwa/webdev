﻿using LinkedListApp;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LinkedListAppTests
{
    public class LinkedList_GetNodeStringTests
    {
        [Fact]
        public void Given_RootNode_ShouldReturn_StringOfAllNodesValues()
        {
            //Arrange
            var root = new LinkedList('E');
            LinkedListHelpers.AddToLast(root, 'A');
            //Act
            var nodeString = LinkedListHelpers.GetNodesString(root);
            //Assert
            Assert.Equal("EA", nodeString);
        }
    }
}
