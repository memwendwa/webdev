﻿using LinkedListApp;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LinkedListAppTests
{
    public class LinkedList_DeleteDuplicatesTests
    {
        [Fact]
        public void GivenRootNodeWithoutDuplicates_ShouldNotRemoveAnyNode()
        {
            //Arrange
            var root = new LinkedList('A');
            LinkedListHelpers.AddToLast(root, 'B');
            LinkedListHelpers.AddToLast(root, 'C');
            //Act
            var stringBeforeDeletingDuplicates = LinkedListHelpers.GetNodesString(root);
            LinkedListHelpers.DeleteDuplicates(root);
            var stringAfterDeletingDuplicates = LinkedListHelpers.GetNodesString(root);
            //Asert

            Assert.Equal("ABC", stringBeforeDeletingDuplicates);
            Assert.Equal(stringBeforeDeletingDuplicates, stringAfterDeletingDuplicates);
        }

        [Fact]
        public void GivenRootNodeWithout3Bs_ShouldRemoveLastB()
        {
            //Arrange
            var root = new LinkedList('B');
            LinkedListHelpers.AddToLast(root, 'B');
            LinkedListHelpers.AddToLast(root, 'B');
            //Act
            var stringBeforeDeletingDuplicates = LinkedListHelpers.GetNodesString(root);
            LinkedListHelpers.DeleteDuplicates(root);
            var stringAfterDeletingDuplicates = LinkedListHelpers.GetNodesString(root);
            //Asert

            Assert.Equal("BBB", stringBeforeDeletingDuplicates);
            Assert.Equal("BB", stringAfterDeletingDuplicates);
        }

        [Fact]
        public void GivenRootNodeWithout5Bs_ShouldRemoveLast3B()
        {
            //Arrange
            var root = new LinkedList('B');
            LinkedListHelpers.AddToLast(root, 'B');
            LinkedListHelpers.AddToLast(root, 'B');
            LinkedListHelpers.AddToLast(root, 'B');
            LinkedListHelpers.AddToLast(root, 'B');
            //Act
            var stringBeforeDeletingDuplicates = LinkedListHelpers.GetNodesString(root);
            LinkedListHelpers.DeleteDuplicates(root);
            var stringAfterDeletingDuplicates = LinkedListHelpers.GetNodesString(root);
            //Asert

            Assert.Equal("BBBBB", stringBeforeDeletingDuplicates);
            Assert.Equal("BB", stringAfterDeletingDuplicates);
        }

        [Fact]
        public void GivenRootNodeWith_EBEEBAB_ShouldReturn_EBEBA()
        {
            //Arrange
            var root = new LinkedList('E');
            LinkedListHelpers.AddToLast(root, 'B');
            LinkedListHelpers.AddToLast(root, 'E');
            LinkedListHelpers.AddToLast(root, 'E');
            LinkedListHelpers.AddToLast(root, 'B');
            LinkedListHelpers.AddToLast(root, 'A');
            LinkedListHelpers.AddToLast(root, 'B');
            //Act
            var stringBeforeDeletingDuplicates = LinkedListHelpers.GetNodesString(root);
            LinkedListHelpers.DeleteDuplicates(root);
            var stringAfterDeletingDuplicates = LinkedListHelpers.GetNodesString(root);
            //Asert

            Assert.Equal("EBEEBAB", stringBeforeDeletingDuplicates);
            Assert.Equal("EBEBA", stringAfterDeletingDuplicates);
        }
    }
}
