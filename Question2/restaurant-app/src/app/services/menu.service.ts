import { Injectable } from '@angular/core';
import { MenuItem } from '../menu-item';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  //menu items collection
  menuItems: MenuItem[] = [
    {
      name: 'Salad',
      choices: [
        { name: 'Santa Fe' },
        { name: 'Greek' },
        { name: 'Asian' }
      ],
      related: [
        {
          name: 'Dressing',
          choices: [
            { name: 'Italian' },
            { name: 'Blue Cheese' },
            { name: 'Panch' }
          ]
        },
        {
          name: 'Bread',
          choices: [
            { name: 'Italian' },
            { name: 'Flat' },
            { name: 'Sourdough' }
          ]
        }
      ]
    },
    {
      name: 'Entree',
      choices: [
        {
          name: 'Steak'
        },
        { name: 'Salmon' },
        { name: 'Rice' }
      ],
      related: []
    },
    {
      name: 'Soup',
      choices: [
        { name: 'Minestrone' },
        { name: 'Hot and Sour' },
        { name: 'Miso' }
      ],
      related: [
        {
          name: 'Bread',
          choices: [
            { name: 'Breadsticks' }
          ],
        }
      ]
    }
  ];


  constructor() { }
//returns list of menu items
  getMenuList(): MenuItem[]{
    return this.menuItems;
  }
}
