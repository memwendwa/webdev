import { Component, OnInit } from '@angular/core';
import { MenuService } from './services/menu.service';
import { MenuItem } from './menu-item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    //get the list of menu items from the menuService
    this.menuItems = this.menuService.getMenuList();
  }
  title = 'restaurant-app';
  menuItems: MenuItem[];
  constructor(private menuService: MenuService){

  }

  //method used to check if any choice has been selected in order to show related menu items
  selectedChoices(item): boolean{
    let i:Number;
    let choiceSelected:boolean = false;
    for(let choice of item.choices){
      if(choice.selected){
        choiceSelected=true;
          break;
      }
    }
    return choiceSelected;
  }

}
