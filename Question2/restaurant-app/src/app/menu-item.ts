import { MenuItemChoice } from './menu-item-choice';

export interface MenuItem {
    name: string;
    choices: MenuItemChoice[];
    related?: MenuItem[];
    selected?: boolean;
}
