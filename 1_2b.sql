Question 1: SQL

SELECT CarMaker, CarModel, SUM(SalePriceInDollar) AS TOtalSalePrice
FROM CarSales 
WHERE SalesDate>= DATEADD(DAY,-30,GETDATE())
GROUP BY CarMaker, CarModel



Question 2b:

Time complexity: O(n), where n is the number of nodes in the linked list
Space complexity: O(n) as well.

